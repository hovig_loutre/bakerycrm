# BakeryCRM
BakeryCRM is an Android application for Customer Relationship Management app, which holds all information of customers of a bakery managed by Bob the Baker.

## Requirements:
- Android Studio 3.0 or higher
- Android SDK Tools 27.0.0 or higher
- Android 8.1 (API 27)

## Steps to test:
- Clone project
- Open the project in Android Studio
- Run the project on an emulator or attached device
 
