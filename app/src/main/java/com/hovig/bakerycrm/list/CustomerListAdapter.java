package com.hovig.bakerycrm.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hovig.bakerycrm.R;
import com.hovig.bakerycrm.models.Customer;
import com.hovig.bakerycrm.models.Purchase;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private TextView tv_full_name;
        private TextView tv_address;
        private TextView tv_last_vsit;
        private Customer customer;

        private ViewHolder(View view) {
            super(view);
            this.view = view;
            tv_full_name = view.findViewById(R.id.tv_full_name);
            tv_address = view.findViewById(R.id.tv_address);
            tv_last_vsit = view.findViewById(R.id.tv_last_vsit);
        }
    }

    private List<Customer> customerList;
    private OnItemClickListener onItemClickListener;
    private SimpleDateFormat formatter;

    public interface OnItemClickListener {
        void OnItemClick(Customer customer);
    }

    public CustomerListAdapter() {
        formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_customer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.customer = customerList.get(position);
        holder.tv_full_name.setText(
                String.format("%s %s", holder.customer.getName(), holder.customer.getLastName()));
        holder.tv_address.setText(holder.customer.getAddress());

        List<Purchase> purchases = holder.customer.getPurchases();
        if (purchases.size() > 0) {
            Purchase lastPurchase = purchases.get(purchases.size() - 1);
            holder.tv_last_vsit.setText(formatter.format(lastPurchase.getDate()));
        } else {
            holder.tv_last_vsit.setText(R.string.never);
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.OnItemClick(holder.customer);
                }
            }
        });
    }

    public void setCustomers(List<Customer> customers) {
        customerList = customers;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (customerList ==  null) {
            return 0;
        }
        return customerList.size();
    }

}
