package com.hovig.bakerycrm.list;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.hovig.bakerycrm.CustomerRepository;
import com.hovig.bakerycrm.models.Customer;

import java.util.List;

public class CustomerListViewModel extends ViewModel {

    private CustomerRepository customerRepository;
    private LiveData<List<Customer>> customers;

     public CustomerListViewModel() {
         super();
         customerRepository = new CustomerRepository();
         customers = customerRepository.getCustomers();
     }

    public LiveData<List<Customer>> getCustomerList() {
        return customers;
    }

    public void addCustomer(Customer customer) {
        customerRepository.insertCustomer(customer);
    }
}
