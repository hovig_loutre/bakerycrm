package com.hovig.bakerycrm.list;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.hovig.bakerycrm.CreateCustomer;
import com.hovig.bakerycrm.R;
import com.hovig.bakerycrm.details.CustomerDetailsActivity;
import com.hovig.bakerycrm.models.Customer;

import java.util.List;

public class CustomerListActivity extends AppCompatActivity {

    public static final int CREATE_CUSTOMER_ACTIVITY_REQUEST_CODE = 1;

    private CustomerListViewModel customerListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab_new_customer = findViewById(R.id.fab_new_customer);
        fab_new_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerListActivity.this, CreateCustomer.class);
                startActivityForResult(intent, CREATE_CUSTOMER_ACTIVITY_REQUEST_CODE);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rv_customer_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        final CustomerListAdapter customerListAdapter = new CustomerListAdapter();
        customerListAdapter.setOnItemClickListener(new CustomerListAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(Customer customer) {
                Intent intent = new Intent(CustomerListActivity.this, CustomerDetailsActivity.class);
                intent.putExtra(CustomerDetailsActivity.EXTRA_CUSTOMER_ID, customer.getId());
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(customerListAdapter);

        customerListViewModel = ViewModelProviders.of(this).get(CustomerListViewModel.class);

        customerListViewModel.getCustomerList().observe(this, new Observer<List<Customer>>() {
            @Override
            public void onChanged(List<Customer> customers) {
                customerListAdapter.setCustomers(customers);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CREATE_CUSTOMER_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Customer customer = data.getParcelableExtra(CreateCustomer.EXTRA_CUSTOMER);
            customerListViewModel.addCustomer(customer);
            Toast.makeText(getApplicationContext(), R.string.customer_add_success, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), R.string.customer_add_failed, Toast.LENGTH_SHORT).show();
        }
    }

}
