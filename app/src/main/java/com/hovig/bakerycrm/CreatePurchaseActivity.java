package com.hovig.bakerycrm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.hovig.bakerycrm.models.Purchase;

import java.util.Calendar;

public class CreatePurchaseActivity extends AppCompatActivity {

    public static final String EXTRA_PURCHASE = "extra_purchase";

    private DatePicker dp_purchase_date;
    private EditText et_purchase_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_purchase);

        dp_purchase_date = findViewById(R.id.dp_purchase_date);
        et_purchase_value = findViewById(R.id.et_purchase_value);

    }

    public void addPurchase(View view) {
        if (!TextUtils.isEmpty(et_purchase_value.getText())) {

            int day = dp_purchase_date.getDayOfMonth();
            int month = dp_purchase_date.getMonth();
            int year =  dp_purchase_date.getYear();
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);

            Purchase purchase = new Purchase(200,
                    calendar.getTime(),
                    Double.valueOf(et_purchase_value.getText().toString()));

            Intent data = new Intent();
            data.putExtra(EXTRA_PURCHASE, purchase);
            setResult(RESULT_OK, data);
            finish();

        } else {
            Toast.makeText(this, R.string.value_empty, Toast.LENGTH_SHORT).show();
        }
    }

}
