package com.hovig.bakerycrm.details;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hovig.bakerycrm.CreatePurchaseActivity;
import com.hovig.bakerycrm.R;
import com.hovig.bakerycrm.models.Customer;
import com.hovig.bakerycrm.models.Purchase;

import java.util.List;

public class CustomerDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_CUSTOMER_ID = "extra_customer_id";
    public static final int CREATE_PURCHASE_ACTIVITY_REQUEST_CODE = 2;

    private CustomerDetailsViewModel customerDetailsViewModel;

    private TextView tv_name;
    private TextView tv_last_name;
    private TextView tv_email;
    private TextView tv_address;
    private TextView tv_spent_last_year;
    private RecyclerView rv_purchase_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tv_name = findViewById(R.id.tv_name);
        tv_last_name = findViewById(R.id.tv_last_name);
        tv_email = findViewById(R.id.tv_email);
        tv_address = findViewById(R.id.tv_address);
        tv_spent_last_year = findViewById(R.id.tv_spent_last_year);
        rv_purchase_list = findViewById(R.id.rv_purchase_list);

        rv_purchase_list.setLayoutManager(new LinearLayoutManager(this));
        rv_purchase_list.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        final PurchaseListAdapter purchaseListAdapter = new PurchaseListAdapter();
        rv_purchase_list.setAdapter(purchaseListAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerDetailsActivity.this,
                        CreatePurchaseActivity.class);
                startActivityForResult(intent, CREATE_PURCHASE_ACTIVITY_REQUEST_CODE);
            }
        });

        int customerId = getIntent().getIntExtra(EXTRA_CUSTOMER_ID, -1);

        customerDetailsViewModel = ViewModelProviders.of(this).get(CustomerDetailsViewModel.class);

        customerDetailsViewModel.setCustomerId(customerId);

        customerDetailsViewModel.getCustomer().observe(this, new Observer<Customer>() {
            @Override
            public void onChanged(Customer customer) {
                tv_name.setText(customer.getName());
                tv_last_name.setText(customer.getLastName());
                tv_email.setText(customer.getEmail());
                tv_address.setText(customer.getAddress());
            }
        });

        customerDetailsViewModel.getLastPurchases().observe(this, new Observer<List<Purchase>>() {
            @Override
            public void onChanged(@Nullable List<Purchase> purchases) {
                purchaseListAdapter.setPurchases(purchases);
            }
        });

        customerDetailsViewModel.getMoneySpentLastYear().observe(this, new Observer<Double>() {
            @Override
            public void onChanged(@Nullable Double moneySpentLastYear) {
                tv_spent_last_year.setText(String.valueOf(moneySpentLastYear));
            }
        });

    }

    public void edit(View view) {
        switch (view.getId()) {
            case R.id.ib_edit_name:
                showInputDialog(R.id.ib_edit_name,
                        R.string.update_name,
                        InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                break;
            case R.id.ib_edit_last_name:
                showInputDialog(R.id.ib_edit_last_name,
                        R.string.update_last_name,
                        InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                break;
            case R.id.ib_edit_email:
                showInputDialog(R.id.ib_edit_email,
                        R.string.update_email,
                        InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            case R.id.ib_edit_address:
                showInputDialog(R.id.ib_edit_address,
                        R.string.update_address,
                        InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS);
                break;
        }
    }

    private void showInputDialog(final int option, int titleResId, int inputType) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titleResId);

        final EditText input = new EditText(this);
        builder.setView(input);
        input.setInputType(inputType);

        builder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handleInput(option, input.getText());
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void handleInput(int resId, CharSequence input) {
        switch (resId) {
            case R.id.ib_edit_name:

                if (TextUtils.isEmpty(input)) {
                    Toast.makeText(getApplicationContext(), R.string.name_empty, Toast.LENGTH_SHORT).show();
                } else {
                    customerDetailsViewModel.updateName(input.toString());
                }
                break;
            case R.id.ib_edit_last_name:
                if (TextUtils.isEmpty(input)) {
                    Toast.makeText(getApplicationContext(), R.string.last_name_empty, Toast.LENGTH_SHORT).show();
                } else {
                    customerDetailsViewModel.updateLastName(input.toString());
                }
                break;
            case R.id.ib_edit_email:
                if (!TextUtils.isEmpty(input) &&
                        !Patterns.EMAIL_ADDRESS.matcher(input).matches()) {
                    Toast.makeText(getApplicationContext(), R.string.email_not_valid, Toast.LENGTH_SHORT).show();
                } else {
                    customerDetailsViewModel.updateEmail(input.toString());
                }
                break;
            case R.id.ib_edit_address:
                customerDetailsViewModel.updateAddress(input.toString());
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CREATE_PURCHASE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Purchase purchase = data.getParcelableExtra(CreatePurchaseActivity.EXTRA_PURCHASE);
            customerDetailsViewModel.addPurchase(purchase);
            Toast.makeText(getApplicationContext(), R.string.purchase_add_success, Toast.LENGTH_SHORT).show();
        }
    }

}
