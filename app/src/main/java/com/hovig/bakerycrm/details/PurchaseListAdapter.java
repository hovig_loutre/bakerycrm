package com.hovig.bakerycrm.details;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hovig.bakerycrm.R;
import com.hovig.bakerycrm.models.Purchase;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class PurchaseListAdapter extends RecyclerView.Adapter<PurchaseListAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_id;
        private TextView tv_date;
        private TextView tv_value;
        private Purchase purchase;

        private ViewHolder(View view) {
            super(view);
            tv_id = view.findViewById(R.id.tv_id);
            tv_date = view.findViewById(R.id.tv_date);
            tv_value = view.findViewById(R.id.tv_value);
        }
    }

    private List<Purchase> purchaseList;
    private SimpleDateFormat formatter;

    public PurchaseListAdapter() {
        formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_purchase, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.purchase = purchaseList.get(position);
        holder.tv_id.setText(String.valueOf(holder.purchase.getId()));
        holder.tv_date.setText(formatter.format(holder.purchase.getDate()));
        holder.tv_value.setText(String.valueOf(holder.purchase.getValue()));
    }

    public void setPurchases(List<Purchase> purchases) {
        purchaseList = purchases;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (purchaseList ==  null) {
            return 0;
        }
        return purchaseList.size();
    }

}
