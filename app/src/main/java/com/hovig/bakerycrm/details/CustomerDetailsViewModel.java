package com.hovig.bakerycrm.details;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.hovig.bakerycrm.CustomerRepository;
import com.hovig.bakerycrm.models.Customer;
import com.hovig.bakerycrm.models.Purchase;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CustomerDetailsViewModel extends ViewModel {

    private static final int LAST_PURCHASE_SIZE = 10;

    private MutableLiveData<Integer> customerId = new MutableLiveData<>();
    private CustomerRepository customerRepository;
    private LiveData<Customer> customer;

    private LiveData<List<Purchase>> lastPurchases;
    private LiveData<Double> moneySpentLastYear;

    public CustomerDetailsViewModel() {
        super();
        customerRepository = new CustomerRepository();

        customer = Transformations.switchMap(customerId, new Function<Integer, LiveData<Customer>>() {
            @Override
            public LiveData<Customer> apply(Integer id) {
                return customerRepository.getCustomer(id);
            }
        });

        lastPurchases = Transformations.map(customer, new Function<Customer, List<Purchase>>() {
            @Override
            public List<Purchase> apply(Customer customer) {
                return filterLastPurchases(customer.getPurchases());
            }
        });

        moneySpentLastYear = Transformations.map(customer, new Function<Customer, Double>() {
            @Override
            public Double apply(Customer customer) {
                return calculateMoneySpentLastYear(customer.getPurchases());
            }
        });
    }

    public void setCustomerId(int id) {
        customerId.setValue(id);
    }

    public LiveData<Customer> getCustomer() {
         return customer;
    }

    public LiveData<List<Purchase>> getLastPurchases() {
        return lastPurchases;
    }

    public LiveData<Double> getMoneySpentLastYear() {
        return moneySpentLastYear;
    }

    private List<Purchase> filterLastPurchases(List<Purchase> purchases) {
        if (purchases.size() <= LAST_PURCHASE_SIZE) {
            return purchases;
        } else {
            return purchases.subList(purchases.size() - LAST_PURCHASE_SIZE, purchases.size());
        }
    }

    private double calculateMoneySpentLastYear(List<Purchase> purchases) {
        double moneySpent = 0;
        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);

        int position = Collections.binarySearch(
                purchases,
                new Purchase(0, lastYear.getTime(), 0),
                new Comparator<Purchase>() {
                    @Override
                    public int compare(Purchase purchase, Purchase purchase1) {
                        return purchase.getDate().compareTo(purchase1.getDate());
                    }
                });

        // if binary search didnt find exact match, it returns negative position which is
        // -(insertion point) - 1
        if (position < 0) {
            position = (position * -1) - 1;
        }

        for (int i = position; i < purchases.size(); i++) {
            moneySpent += purchases.get(i).getValue();
        }

        return Math.round(moneySpent * 100.0) / 100.0;
    }

    public void updateName(String name) {
        Customer customer = this.customer.getValue();
        customer.setName(name);
        customerRepository.updateCustomer(customer);
    }

    public void updateLastName(String lastName) {
        Customer customer = this.customer.getValue();
        customer.setLastName(lastName);
        customerRepository.updateCustomer(customer);
    }

    public void updateEmail(String email) {
        Customer customer = this.customer.getValue();
        customer.setEmail(email);
        customerRepository.updateCustomer(customer);
    }

    public void updateAddress(String address) {
        Customer customer = this.customer.getValue();
        customer.setAddress(address);
        customerRepository.updateCustomer(customer);
    }

    public void addPurchase(Purchase purchase) {
        Customer customer = this.customer.getValue();
        customer.getPurchases().add(purchase);
        customerRepository.updateCustomer(customer);
    }

}
