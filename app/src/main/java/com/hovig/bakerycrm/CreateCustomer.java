package com.hovig.bakerycrm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.hovig.bakerycrm.models.Customer;
import com.hovig.bakerycrm.models.Purchase;

import java.util.ArrayList;

public class CreateCustomer extends AppCompatActivity {

    public static final String EXTRA_CUSTOMER = "extra_customer";

    private EditText et_name;
    private EditText et_last_name;
    private EditText et_email;
    private EditText et_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_customer);

        et_name = findViewById(R.id.et_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_email = findViewById(R.id.et_email);
        et_address = findViewById(R.id.et_address);
    }

    public void addCustomer(View view) {
        if (isFieldsValid()) {
            String name = et_name.getText().toString();
            String lastName = et_last_name.getText().toString();
            String email = et_email.getText().toString();
            String address = et_address.getText().toString();

            Customer customer = new Customer(
                    name,
                    lastName,
                    email,
                    address,
                    new ArrayList<Purchase>()
            );

            Intent data = new Intent();
            data.putExtra(EXTRA_CUSTOMER, customer);
            setResult(RESULT_OK, data);
            finish();
        }
    }

    private boolean isFieldsValid() {
        if (TextUtils.isEmpty(et_name.getText()) || TextUtils.isEmpty(et_last_name.getText())) {
            Toast.makeText(getApplicationContext(), R.string.customer_info_not_filled,
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!TextUtils.isEmpty(et_email.getText()) &&
                !Patterns.EMAIL_ADDRESS.matcher(et_email.getText()).matches()) {
            Toast.makeText(getApplicationContext(), R.string.email_not_valid,
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
