package com.hovig.bakerycrm;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;

import com.hovig.bakerycrm.models.Customer;
import com.hovig.bakerycrm.models.Purchase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CustomerRepository {

    private static final int MOCK_CUSTOMERS_SIZE = 10;
    private static final int MOCK_PURCHASES_SIZE = 15;
    private static int MOCK_CUSTOMER_ID = 0;

    private static MutableLiveData<List<Customer>> customers = new MutableLiveData<>();

    public CustomerRepository() {

        if (customers.getValue() == null) {
            customers.setValue(new ArrayList<Customer>());
            createMockCustomers();
        }

    }

    public LiveData<List<Customer>> getCustomers() {
        return customers;
    }

    public LiveData<Customer> getCustomer(final int id) {
        return Transformations.map(customers, new Function<List<Customer>, Customer>() {
            @Override
            public Customer apply(List<Customer> customersList) {
                for (int i = 0; i < customersList.size(); i++) {
                    if (customersList.get(i).getId() == id) {
                        return customersList.get(i);
                    }
                }
                return null;
            }
        });
    }

    public void insertCustomer(Customer customer) {
        customer.setId(MOCK_CUSTOMER_ID);
        List<Customer> customersList = customers.getValue();
        customersList.add(customer);
        customers.setValue(customersList);
        MOCK_CUSTOMER_ID++;
    }

    public void updateCustomer(Customer customer) {
        List<Customer> customersList = customers.getValue();
        for (int i = 0; i < customersList.size(); i++) {
            if (customersList.get(i).getId() == customer.getId()) {
                customersList.set(i, customer);
                customers.setValue(customersList);
                return;
            }
        }
    }

    private void createMockCustomers() {
        Customer customer;
        for (int i = 0; i < MOCK_CUSTOMERS_SIZE; i++) {
            customer = new Customer(
                    "name" + i,
                    "lastName" + i,
                    "email" + i + "@gmail.com",
                    "address" + i,
                    createMockPurchases(i)
            );
            insertCustomer(customer);
        }
    }

    private List<Purchase> createMockPurchases(int customerId) {
        List<Purchase> purchases = new ArrayList<>();
        Purchase purchase;
        Calendar c = Calendar.getInstance();
        double value;
        for (int i = 0; i < MOCK_PURCHASES_SIZE; i++) {
            value = 1 + (Math.random() * 20);
            value = Math.round(value * 100.0) / 100.0;
            purchase = new Purchase(
                    (customerId * 10) + i,
                    c.getTime(),
                    value
            );
            purchases.add(purchase);
            c.add(Calendar.DATE, 1);
        }
        return purchases;
    }
}
