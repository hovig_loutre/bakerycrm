package com.hovig.bakerycrm.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Purchase implements Parcelable {

    private int id;
    private Date date;
    private double value;

    public Purchase(int id, Date date, double value) {
        this.id = id;
        this.date = date;
        this.value = value;
    }

    protected Purchase(Parcel in) {
        id = in.readInt();
        Date parcelDate = new Date();
        parcelDate.setTime(in.readLong());
        date = parcelDate;
        value = in.readDouble();
    }

    public static final Creator<Purchase> CREATOR = new Creator<Purchase>() {
        @Override
        public Purchase createFromParcel(Parcel in) {
            return new Purchase(in);
        }

        @Override
        public Purchase[] newArray(int size) {
            return new Purchase[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeLong(date.getTime());
        parcel.writeDouble(value);
    }

    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public double getValue() {
        return value;
    }

}
