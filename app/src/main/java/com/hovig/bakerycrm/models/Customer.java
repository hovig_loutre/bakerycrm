package com.hovig.bakerycrm.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Customer implements Parcelable {
    private int id;
    private String name;
    private String lastName;
    private String email;
    private String address;
    private List<Purchase> purchases;

    public Customer(String name, String lastName, String email, String address,
                    List<Purchase> purchases) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.purchases = purchases;
    }

    protected Customer(Parcel in) {
        name = in.readString();
        lastName = in.readString();
        email = in.readString();
        address = in.readString();
        purchases = in.createTypedArrayList(Purchase.CREATOR);
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(lastName);
        parcel.writeString(email);
        parcel.writeString(address);
        parcel.writeTypedList(purchases);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<Purchase> purchases) {
        this.purchases = purchases;
    }

}
